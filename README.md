# Annotated Maps

This is a repo of some annotated maps I make. So far, there is only a map of
Dallas, but I plan to add more.

Basically, they all use the [ai2html] script by The New York Times to make
responsive images from Adobe Illustrator files.

I based my work off [this tutorial][PointsUnknown tutorial].


[ai2html]: http://ai2html.org/
[PointsUnknown tutorial]: https://pointsunknown.github.io/tutorials/03_AnnotationLayer.html
